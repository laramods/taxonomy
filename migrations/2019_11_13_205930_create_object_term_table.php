<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObjectTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object_term', function (Blueprint $table) {

            $table->unsignedBigInteger('term_id')->index();

            $table->string('object_type');
            $table->unsignedBigInteger('object_id');

            $table->unique(['object_type', 'object_id', 'term_id']);

            $table->foreign('term_id')
                ->references('id')->on('terms')
                ->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('object_term');
    }
}

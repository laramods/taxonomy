<?php

namespace Laramods\Taxonomy\Providers;


use Illuminate\Support\ServiceProvider;
use Laramods\Taxonomy\Taxonomy;

class TaxonomyServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){

        $this->loadMigrationsFrom(__DIR__.'/../../migrations');

        $this->publishes([
            __DIR__.'/../../migrations' => database_path('migrations'),
        ], ['laramods', 'laramods_taxonomy', 'laramods_taxonomy_migrations', 'migrations']);


        $this->publishes([
            __DIR__.'/../../config/taxonomy.php' => config_path('laramods/taxonomy.php'),
        ], ['laramods', 'laramods_taxonomy', 'laramods_taxonomy_config', 'config']);


    }



    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function register()
    {

        $this->mergeConfigFrom(
            __DIR__.'/../../config/taxonomy.php', 'laramods.taxonomy'
        );

        foreach ( config('laramods.taxonomy.taxonomies') as $taxonomy => $definition ){
            Taxonomy::add($taxonomy, $definition);
        }

    }


}

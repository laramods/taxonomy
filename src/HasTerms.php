<?php

namespace Laramods\Taxonomy;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphToMany;


/**
 * Trait HasTerms
 * @package Laramods\Taxonomy
 *
 * @property Term[]|Collection $terms
 */
trait HasTerms
{

    /**
     * @return MorphToMany
     */
    public function terms(){
        return $this->morphToMany( Taxonomy::getTermModelClass(), 'object', 'object_term' );
    }

}
